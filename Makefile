exec: programme.o
	gcc -o exec programme.o 

programme.o : src/programme.c
	gcc -o programme.o -c -Wall src/programme.c

clean: 
	rm -f *.o
	rm -f exec