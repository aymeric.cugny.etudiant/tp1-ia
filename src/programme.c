#include <stdbool.h>     
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 

#define TAILLE 3

void display(unsigned int taquin[TAILLE][TAILLE]) 
{
    for(unsigned int i = 0; i < TAILLE; i++)
    {
        for(unsigned int j = 0; j < TAILLE; j++)
        {
            if(taquin[i][j] == 0)
            {printf("  ");} 
            else {printf("%d ", taquin[i][j]);} 
        }
        printf("\n"); 
    }
}

bool est_but(unsigned int test[TAILLE][TAILLE], unsigned int but[TAILLE][TAILLE]) 
{
    unsigned compteur = 0; 
    for(unsigned int i = 0; i < TAILLE; i++)
    {
        for(unsigned int j = 0; j < TAILLE; j++)
        {
            if(test[i][j] == but[i][j])
            {compteur++;}
        }
    }
    bool out = (compteur == TAILLE * TAILLE) ? true : false;
    if(out) {printf("oui\n");} else {printf("non\n");} 
    return out; 
}

bool est_solvable(unsigned int taquin[TAILLE][TAILLE])
{
    unsigned int nb_inversion = 0; 
    unsigned int position_i = 0;
    for(unsigned int i = 0; i < TAILLE; i++)
    {
        for(unsigned int j = 0; j < TAILLE; j++)
        { 
            if(taquin[i][j] != 0)
            { 
                printf("%d ", taquin[i][j]); 
                do {
                    for(unsigned int position_j = i + 1; position_j < TAILLE; position_j++)
                    {
                        printf(" compared by %d\n", taquin[position_i][position_j]); 
                        if(taquin[i][j] > taquin[position_i][position_j] && taquin[position_i][position_j] != 0) 
                        {
                            nb_inversion++; 
                        }
                    }
                    position_i++;  
                } while(position_i < TAILLE);
                position_i = 0;    
            }
        }
    }
    bool out = (nb_inversion%2 ==0) ? true : false; 
    if(out) {printf("oui\n");} else {printf("non\n");}
    printf("compteur : %d\n", nb_inversion); 
    return out; 
}

int main()
{
    unsigned int but[TAILLE][TAILLE] = {{0,1,2}, {3,4,5}, {6,7,8}}; 
    unsigned int test[TAILLE][TAILLE] = {{6,5,3}, {2,1,7}, {8,4,0}}; 

    printf("le taquin que nous devons tester :\n"); 
    display(test);
    printf("le taquin que nous devons avoir :\n"); 
    display(but);
    printf("le taquin est-il un taquin but ? : "); 
    est_but(test, but);
    printf("le taquin est-il un taquin solvable ? : ");
    est_solvable(test);  

    return 0; 
}